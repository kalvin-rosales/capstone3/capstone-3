const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
	title: {
	    type: String,
	    required: [true, `Product name is required`],

	},
	desc: {
	    type: String,
	    required: [true, `Product description is required`]
	},
	img: {
	    type: String,
	    required: [true, `Product image is required`]
	},
	price: {
	    type: Number,
	    required: [true, `Price is required`]
	},
	categories: {
	    type: Array,
	   
	},    
	size:{
			type:Array, 
	
	
	},
	isActive: {
	    type: Boolean,
	    default: true
	},
	createdOn: {
	    type: Date,
	    default: new Date()
	},
	// orderedProducts: [
	//     {
	//         // orderId: {
	//         //     type: String,
	//         //     required: [true, `orderId is required`]
	//         // },
	        

	//         orderedOn: {
	//             type: Date,
	//             default: new Date()
	//         }
	//     }
	// ]

}, {timestamps: true})

module.exports = mongoose.model("Product", productSchema);