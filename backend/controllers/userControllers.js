const CryptoJS = require("crypto-js");
const User = require('./../models/User')
const Order = require('./../models/Order')

const {createToken} = require('./../auth');

module.exports.register = async (reqBody) => {
	// console.log(reqBody)
	const {username,firstName, lastName, email, password} = reqBody

	const newUser = new User({
		username:username,
		firstName: firstName,
		lastName: lastName, 
		email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
		//password: bcrypt.hashSync(password, 10)
	})

	return await  newUser.save().then(result => {
		if(result){
			return result
		} else {
			if(result == null){
				return false
			}
		}
	})
}

module.exports.checkEmail = async (reqBody) => {
	const{email} = reqBody

	return await User.findOne({email:email}).then((result,err) =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			} else {
				return err
			}
		}
	})
}

module.exports.getAllUsers = async () => {

	return await User.find().then(result => result)
}






module.exports.profile = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message:`user does not exist`}
			} else {
				return err
			}
		}
	})
}


module.exports.login = async (reqBody) =>{

	return await User.findOne({email: reqBody.email}).then((result, err)=> {
		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				//check if password is correct
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				console.log(reqBody.password==decryptedPw)

				if(reqBody.password == decryptedPw){
						//create a token for the user

						return {token:createToken(result)}			
				} else {
						return {auth: `Auth Failed`}
				}

			} else {
				return err
			}
		}
	})
}

module.exports.adminStatus = async (id,reqBody) => {
	

	return await User.findByIdAndUpdate(id, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
}

module.exports.adminStatusUser = async (id,reqBody) => {
	

	return await User.findByIdAndUpdate(id, {$set: {isAdmin: false}}, {new:false}).then((result, err) => result ? false : err)
}

module.exports.deleteUser = async (deleteId) => {

	return await User.findByIdAndDelete(deleteId).then((result, err) => result ? `You have deleted the user` : err)
}







module.exports.myorders = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message:`user does not exist`}
			} else {
				return err
			}
		}
	})
}




module.exports.update = async (userId,reqBody) => {
	const userData ={

		email: reqBody.email,
		password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
	}

	return await User.findByIdAndUpdate(userId,{$set:userData},{new:true}).then((result,err) =>{	
		if(result){
			result.password = "***"
			return result
		}else{
			return err
		}
	})
}







module.exports.checkout = async (data) =>{
	const{userId, orderId} = data

	const updatedUser = await User.findById(userId).then(result => {
		result.orders.push({orderId: orderId})
		
		 return result.save().then(user => user ? true : false)
	})


	const updatedOrder = await Order.findById(orderId).then(result => {
		result.incomingOrders.push({userId:userId})
	
		 return result.save().then(order => order ? true : false)
	})


	if(updatedUser && updatedOrder){
		return true
	} else {
		false
	}
}

module.exports.updatePw = async (id, password) => {
	let updatedPw = {
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
	}

	return await User.findByIdAndUpdate(id, {$set: updatedPw})
	.then((result, err) => {
		if(result){
			result.password = "***"
			return result
		} else{
			return err
		}
	})
}


