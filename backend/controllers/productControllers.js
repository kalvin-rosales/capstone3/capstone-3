const Product = require('./../models/Product');





module.exports.create = async (reqBody) => {
	const {title, desc, price, img, categories, size} = reqBody

	let newProduct = new Product({
		title: title,
		desc: desc,
		price: price,
		img: img,
		categories: [categories],
		size: [size],
	})

	return await newProduct.save().then((result, err) => result ? result : err)
}


module.exports.activeProducts = async () => {

	return await Product.find({isOffered:true}).then(result => result)
}


module.exports.getAProduct = async (id) => {

	return await Product.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message: `Product not found`}
			}else{
				return err
			}
		}
	})
}

module.exports.updateProduct = async (productId, reqBody) => {

	return await Product.findByIdAndUpdate(productId, {$set: reqBody}, {new:true}).then(result => result)
}


module.exports.archive = async (productId) => {

	return await Product.findByIdAndUpdate(productId, {$set: {isActive: false}}, {new:true}).then(result => result)
}

module.exports.unArchive = async (productId) => {

	return await Product.findByIdAndUpdate(productId, {$set: {isActive: true}}, {new:true}).then(result => result)
}


module.exports.deleteProduct = async (deleteId) => {

	return await Product.findByIdAndDelete(deleteId).then((result, err) => result ? `You have deleted the product` : err)
}

module.exports.getAllProducts = async () => {
// console.log('h')
	return await Product.find().then(result => result)
}