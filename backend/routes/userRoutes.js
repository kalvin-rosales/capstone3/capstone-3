const express = require('express');
const router = express.Router();

const Order = require('./../models/Order')

const {
	register,
	checkEmail,
	getAllUsers,
	profile,
	login,
	adminStatus,
	adminStatusUser,
	deleteUser,
	myorders,
	update,
	updatePw,
	checkout
	} = require('./../controllers/userControllers');

const {verify, decode, verifyAdmin} = require('./../auth');


router.post('/register', async (req, res) => {
	 //console.log(req.body)	

	try{
		await register(req.body).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})

router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})



router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



router.get('/profile', verify, async (req, res) => {
	// console.log(req.headers.authorization)
	const userId = decode(req.headers.authorization).id
	// console.log(userId)

	try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.post('/login', (req, res) => {
	// console.log(req.body)
	try{
		login(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.put('/:userId/setAsAdmin', verifyAdmin, async (req, res) => {
	const id = req.params.userId
	try{
		await adminStatus(id, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

router.put('/:userId/setAsUser', verifyAdmin, async (req, res) => {
	const id = req.params.userId
	try{
		await adminStatusUser(id, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})

router.delete('/:userId/delete-user', verifyAdmin, async (req, res) => {
	const deleteid = req.params.userId
	try{
		await deleteUser(deleteid).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



router.get('/myorders', verify, async (req, res) => {

	const userId = decode(req.headers.authorization).id


	try{
		myorders(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.put('/update', verify, async (req, res) => {
	

	const userId = decode(req.headers.authorization).id
	try{
		await update(userId, req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

router.put('/update-password', verify, async (req, res) => {
	console.log( decode(req.headers.authorization).id )
	console.log(req.body.password)

	const userId = decode(req.headers.authorization).id
	try{
		await updatePw(userId, req.body.password).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})






router.post('/checkout',verify, async (req,res) => {
	const user = decode(req.headers.authorization).isAdmin
	const data = {
		userId: decode(req.headers.authorization).id,
		orderId: req.body.orderId
	}

	if(user == false){
		try{
			await checkout(data).then(result => res.send(result))
		} catch(err){
			res.status(500).json(err)
		}
	} else {
		res.send(`Only with user access can checkout`)
	 }
})





module.exports = router;