 const express = require('express')
const router = express.Router()

const User = require('./../models/User')
const Product = require('./../models/Product')
const Order = require('./../models/Order')

const { create,
	getAOrder,
	getAllOrder,
	deleteOrder,
	order,
	updateOrder

}= require('./../controllers/orderControllers')

const {verifyAdmin, decode,verify} = require('./../auth')

router.post('/create',verify, async (req, res) => {
	// console.log(req.body)
	const user = decode(req.headers.authorization).isAdmin
	if(user == false){
	try{
		create(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
	} else {
		res.send(`Only with user access can create an order`)
	}
})


// router.post("/", verify, async (req, res) => {
//   const newOrder = new Order(req.body);

//   try {
//     const savedOrder = await newOrder.save();
//     res.status(200).json(savedOrder);
//   } catch (err) {
//     res.status(500).json(err);
//   }
// });


// router.post('/create',verify, async (req,res) => {
// 	const user = decode(req.headers.authorization).isAdmin
// 	const data = {
// 		productId: req.body.productId,
// 		orderId: req.body.orderId
// 	}

// 	if(user == false){
// 		try{
// 			await create(data).then(result => res.send(newOrder))
// 		} catch(err){
// 			res.status(500).json(err)
// 		}
// 	} else {
// 		res.send(`Only with user access can create an order`)
// 	 }
// })

















router.get('/:orderId', verify, async (req, res) => {
	try{
		await getAOrder(req.params.orderId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.get('/', verifyAdmin, async (req, res) => {

	try{
		await getAllOrder().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.delete('/:orderId/delete-order', verifyAdmin, async (req, res) => {
	const deleteid = req.params.orderId
	try{
		await deleteOrder(deleteid).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




router.put('/:orderId/update', verify, async (req, res) => {
	const id = req.params.userId
	try{
		await updateOrder(id, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})













router.post('/checkout',verify, async (req,res) => {
	const user = decode(req.headers.authorization).isAdmin
	const data = {
		userId: decode(req.headers.authorization).id,
		orderId: req.body.orderId
	}

	if(user == false){
		try{
			await checkout(data).then(result => res.send(result))
		} catch(err){
			res.status(500).json(err)
		}
	} else {
		res.send(`Only with user access can checkout`)
	 }
})





module.exports = router