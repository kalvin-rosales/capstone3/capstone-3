const express = require('express')
const router = express.Router()
const Product = require("../models/Product")

const {create,
	activeProducts,
	getAProduct,
	updateProduct,
	archive,
	unArchive,
	deleteProduct,
	getAllProducts

} = require('./../controllers/productControllers')

const {verifyAdmin, decode, verify} = require('./../auth')


router.post('/createproduct', verifyAdmin, async (req, res) => {
	// console.log(req.body)
	const admin = decode(req.headers.authorization).isAdmin

	if(admin){
	try{
		create(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
	} else {
		return false
	}
})

router.get('/all', verifyAdmin, async (req, res) => {

	try{
		await getAllProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})





router.get('/activeproducts', verify, async (req, res) => {
	try{
		await activeProducts().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.get('/:productId', verify, async (req, res) => {
	try{
		await getAProduct(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.put('/:productId/update', verifyAdmin, async (req, res) => {
	try{
		await updateProduct(req.params.productId, req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.put('/:productId/archive', verifyAdmin, async (req, res) => {
	try{
		await archive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.put('/:productId/unarchive', verifyAdmin, async (req, res) => {
	try{
		await unArchive(req.params.productId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


router.delete('/:productId/delete-product', verifyAdmin, async (req, res) => {
	const deleteid = req.params.productId
	try{
		await deleteProduct(deleteid).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

router.get("/", async (req,res) =>{
	const qNew = req.query.new;
	const qCategory = req.query.category;

	try{
		let product;
		if(qNew){
			products = await Product.find({isOffered:true}).sort({createdAt: -1}).limit(5)
		} else if(qCategory){
			product = await Product.find({
				categories:{
					$in: [qCategory],
				},
			});
		} else {
			product = await Product.find();
		}


		res.status(200).json(product);
	}catch(err){
		res.status(500).json(err)
	}
})


module.exports = router