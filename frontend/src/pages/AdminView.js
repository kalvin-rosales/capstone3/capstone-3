import { useEffect, useState, Fragment, useContext } from 'react'
import { Container, Table, Button } from 'react-bootstrap'
import UserContext from './../UserContext'
import{useNavigate} from 'react-router-dom'

import AddProduct from './AddProduct'



export default function AdminView() {
	const navigate = useNavigate();

	const [allProducts, setAllProducts] = useState([])

	const { dispatch } = useContext(UserContext)

	const fetchData = () => {
		fetch(`http://localhost:4000/api/product/all`, {
			method: "GET",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			dispatch({type: "USER", payload: true})

			setAllProducts( response.map(product => {
				// console.log(product)

				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.title}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								product.isActive ?
									<Button 
										className="btn btn-danger mx-2"
										onClick={ () => handleArchive(product._id) }
									>
										Archive
									</Button>
								:
									<Fragment>
										<Button 
											className="btn btn-success mx-2"
											onClick={ () => handleUnarchive(product._id)}
										>
												Unarchive
										</Button>
										<Button 
											className="btn btn-secondary mx-2"
											onClick={ () => handleDelete(product._id) }
										>
											Delete
										</Button>
									</Fragment>
							}
						</td>
					</tr>
				)
			}))
		})
	}

	useEffect(() => {
		fetchData()

	}, [])

	const handleArchive = (productId) =>{
		console.log(productId)
		
		fetch(`http://localhost:4000/api/product/${productId}/archive`, {
			method: "PUT",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()

				alert('Product successfully archived!')
			}
		})
	}

	const handleUnarchive = (productId) =>{
		console.log(productId)
		fetch(`http://localhost:4000/api/product/${productId}/unarchive`, {
			method: "PUT",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('Product successfully Unarchived!')
			}
		})
	}

	const handleDelete = (productId) =>{
		console.log(productId)
		fetch(`http://localhost:4000/api/product/${productId}/delete-product`, {
			method: "DELETE",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('Product successfully Deleted!')
			}
		})
	}
	const handleClickAdd = () => {
        navigate('/addproduct')
    };
	const handleClickUser = () => {
        navigate('/adminviewuser')
    };

	return(
		<Container className="container">
			<h1 className="my-5 text-center">Admin Dashboard</h1>
			<Button className="btn btn-success mx-2" onClick={handleClickAdd}>Add Product</Button>
			<Button className="btn btn-success mx-2" onClick={handleClickUser}>User Dashboard</Button>
			<Table>
				<thead>
					<tr>
						<th>ID</th>
						<th>Product Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ allProducts }
				</tbody>
			</Table>
		</Container>
	)
}