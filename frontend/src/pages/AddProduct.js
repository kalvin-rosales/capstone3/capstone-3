import { useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Form, Button } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'
import styled from "styled-components";

import UserContext from './../UserContext'

const token = localStorage.getItem('token')


const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background: 
    url("https://wallpaperaccess.com/full/1076784.jpg")
      center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;


export default function CreateProduct(){


	// update the user state using context

	// add the course by getting the user input and send it as a client request via fetch
	// send an alert once added course succesfully
	// navigate back to courses dashboard
	// course newly added must show in the dashboard at the very end of the courses

	const [title, setTitle] = useState('')
	const [desc, setDesc] = useState('')
    const [img, setImg] = useState('')
    const [cat, setCat] = useState('')
    const [size, setSize] = useState('')
	const [price, setPrice] = useState(0)

	const navigate = useNavigate()

	const { dispatch } = useContext(UserContext)


	useEffect(() => {
		if(token !== null){

			dispatch({type: "USER", payload: true})
		}
	}, [])


	const handleSubmit = (e) => {
		e.preventDefault()

		fetch('http://localhost:4000/api/product/createproduct',{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				title: title,
				desc: desc,
                img: img,
                cat: cat,
                size: size,
				price: price
			})
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)
			if(response){

				alert('Product successfully created!')

				navigate('/products')
			}
		})
	}

	return(
        <Wrapper>
		<Container className="container m-5">
		 	<h1 className="text-center text-light">Add Product</h1>
			<Form onSubmit={ (e) => handleSubmit(e) }>
				<Row>
					<Col xs={12} md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Product Name"
					    		type="text" 
					    		value={title}
					    		onChange={ (e) => setTitle(e.target.value) }
					    		
					    	/>
						</Form.Group>
					</Col>
					<Col xs={12}  md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Description"
					    		type="text" 
					    		value={desc}
					    		onChange={ (e) => setDesc(e.target.value) }
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Row>
					<Col xs={12}  md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Image"
					    		type="img" 
					    		value={img}
					    		onChange={ (e) => setImg(e.target.value) }
					    		
					    	/>
						</Form.Group>
					</Col>
                    <Col xs={12} md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Categories"
					    		type="array" 
					    		value={cat}
					    		onChange={ (e) => setCat(e.target.value) }
					    		
					    	/>
						</Form.Group>
					</Col>
                    <Col xs={12} md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Size"
					    		type="array" 
					    		value={size}
					    		onChange={ (e) => setSize(e.target.value) }
					    		
					    	/>
						</Form.Group>
					</Col>
                    <Col xs={12} md={12}>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Price"
					    		type="number" 
					    		value={price}
					    		onChange={ (e) => setPrice(e.target.value) }
					    		
					    	/>
						</Form.Group>
					</Col>
				</Row>
				<Button type="submit" className="btn btn-info btn-block">Create Product</Button>
			</Form>
		</Container>
        </Wrapper>
	)
}
