import { useContext, useEffect } from 'react'
import UserContext from './../UserContext'
import { Navigate } from 'react-router-dom'
const token = localStorage.getItem('token')

export default function Logout(){
	
	const { state, dispatch } = useContext(UserContext)

	useEffect( () => {

 		localStorage.clear()

		if(token){
			dispatch({type: "USER", payload: true})
		} else {
			dispatch({type: "USER", payload: null})
		}
	}, [])

	return(
		<Navigate to="/login" />
	)
}
