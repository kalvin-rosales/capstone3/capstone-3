import React from 'react'
import styled from 'styled-components'
import Announcement from '../components/Announcement'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import { Add, Remove } from '@material-ui/icons'
import {useLocation} from 'react-router-dom'
import { useEffect } from 'react'
import {useState} from 'react'
import {publicRequest} from "../requestMethods"
import { addProduct } from '../redux/cartRedux'
import { useDispatch } from 'react-redux'
const token = localStorage.getItem('token')

const Container = styled.div`

`


const Wrapper = styled.div`
  padding: 50px;
  display: flex
`


const ImgContainer = styled.div`
  flex:1;
  justify-content:center;
`


const Image = styled.img`
  width: 100%
  height: 90vh;
  object-fit:cover;
  
`


const InfoContainer = styled.div`
  flex:1;
  padding: 0px; 50px;
`


const Title = styled.h1`
    font-weight:200;
`

const Desc = styled.p`
  margin: 20px 0px;
`

const Price = styled.span`
  font-weight: 100;
  font-size: 40px;
`

const FilterContainer = styled.div`
  width: 50%;
  margin: 30px 0px;
  display:flex;
  justify-content: space-between;
`


const Filter = styled.div`
  display:flex;
  align-items:center;

`


const FilterTitle = styled.div`
      font-size:20px;
      font-weight:200;
`


const FilterSize = styled.select`
    margin-left: 10px;


`


const FilterSizeOption = styled.option``


const FilterGender = styled.select`
    margin-left: 10px;
`


const FilterGenderOption = styled.option``


const AddContainer = styled.div`
  width: 50%;
  display:flex;
  align-items:center;
  justify-content: space-between;


`
const AmountContainer = styled.div`
  display:flex;
  align-items:center;
  font-weight: 700;
  cursor: pointer;
  transition:all 1s ease;
  &:hover{
    
    transform: scale(2);
  }

`
const Amount = styled.span`
  width: 30px;
  height:30px;
  border-radius: 50%;
  border: 1px  black;
  display:flex;
  align-items: center;
  justify-content: center;
  margin: 0px 5px;
`
const Button = styled.button`
  padding: 15px;
  border: 15px;
  font-weight: 700;
  border-radius: 75%;
  background-color: red;
  cursor: pointer;
  transition:all 1s ease;
  &:hover{
    background-color:pink;
    transform: scale(2);
  }

`



const Product = () => {
  const location = useLocation();
  console.log(location.pathname.split("/")[2]);

  const id = location.pathname.split("/")[2];

    const [product,setProduct] = useState({});

    const[quantity, setQuantity] = useState(1);

    const [size, setSize] = useState("");

    const [gender, setGender] = useState("");

    const dispatch = useDispatch()

    useEffect(() => {
      const getProduct = async () => {
        try {
          const res = await publicRequest.get("/product/" + id,{
          headers:{
            "Authorization": `Bearer ${token}`
          }})
          setProduct(res.data);
        } catch {}
      };
      getProduct();
    }, [id]);


    const handleQuantity = (type) => {
      if( type == "decrease"){
        quantity > 1 && setQuantity( quantity -1)
      } else {
        setQuantity( quantity +1)
      }
    }


    const handleClick = () => {

      dispatch(
      addProduct({...product, quantity, size, gender})
      )
      
      
    };



  return (
    <Container>
        <Announcement />
        <Navbar />
          <Wrapper>

          
            <ImgContainer>
              <Image src={product.img}/>
            </ImgContainer>
            <InfoContainer>
              <Title>{product.title}</Title>
              <Desc>{product.desc}</Desc>
              <Price>{product.price}</Price>
              <FilterContainer>
                <Filter>
                  <FilterTitle>Size</FilterTitle>
                    <FilterSize onChange={(e)=> setSize(e.target.value)}>
                        <FilterSizeOption>
                          Small
                        </FilterSizeOption>
                        <FilterSizeOption>
                          Medium
                        </FilterSizeOption>
                        <FilterSizeOption>
                          Large
                        </FilterSizeOption>
                    </FilterSize>
                </Filter>
                <Filter>
                  <FilterTitle>Gender</FilterTitle>
                    <FilterGender onChange={(e)=> setSize(e.target.value)}>
                        <FilterGenderOption>
                          Men
                        </FilterGenderOption>
                        <FilterGenderOption>
                          Women
                        </FilterGenderOption>
                    </FilterGender>
                </Filter>
              </FilterContainer>
              <AddContainer>

                <AmountContainer>
                  <Remove onClick ={() => handleQuantity("decrease")}/>
                  <Amount>{quantity}</Amount>
                  <Add onClick ={() => handleQuantity("increase")}/>
                </AmountContainer>

                <Button onClick={handleClick}>ADD TO CART</Button>
              </AddContainer>
            </InfoContainer>
          </Wrapper>

        <Footer />
    </Container>
  )
}

export default Product
