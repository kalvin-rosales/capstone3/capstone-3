import React from 'react'
import {useState, useEffect} from 'react'
import{useNavigate} from 'react-router-dom'
import {Link} from 'react-router-dom';
import styled from "styled-components";


const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: 
    url("https://www.etftrends.com/wp-content/uploads/2019/05/Footwear-Companies-Concerned-About-Tariffs.jpg")
      center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 40%;
  padding: 20px;
  background-color: white;

`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
`;

const Input = styled.input`
  flex: 1;
  min-width: 100%;
  margin: 20px 10px 0px 0px;
  padding: 10px;
`;

const Linek = styled.a`
  margin-top: 30px;
  font-size: 20px;
  text-decoration: underline;
  cursor: pointer;
`;

const Button = styled.button`
    margin-top: 50px;   
  width: 100%;
  border: none;
  padding: 15px 20px;
  background-color: red;
  color: black;
  cursor: pointer;
  transition:all 1s ease;
  &:hover{
    background-color:pink;
    transform: scale(1.5);
  }
`;

const Register = () => {

  const [username, setUsername] = useState("")
  const [fN, setFN] = useState("")
	const [lN, setLN] = useState("")
	const [email, setEmail] = useState("")
	const [pw, setPW] = useState("")
	const [vpw, setVPW] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)


	const navigate = useNavigate();

	// useEffect(function, options)
	useEffect(() => {
		console.log('render')

		// if all fields are filled out and pw & vpw is equal, change the state to false
		if((username !== "" && fN !== "" && lN !== "" && email !== "" && pw !== "" && vpw !== "") && (pw == vpw)){

			setIsDisabled(false)

		} else {
			//if all input fields are empty, keep the state of the button to true
			setIsDisabled(true)
		}

		//listen to state changes: fn, ln, em, pw, vf
	}, [username, fN, lN, email, pw, vpw])

	const registerUser = (e) => {
		e.preventDefault()

		fetch('http://localhost:4000/api/user/email-exists',{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		}).then(response => response.json())
		.then(response => {
			//console.log(response) //false
			if(!response){
				//send request to register
				fetch('http://localhost:4000/api/user/register',{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
        username: username,
				firstName: fN,
				lastName: lN,
				email: email,
				password: pw
			})
		})
		.then(response => response.json())
		.then(response =>{
			//console.log(response)

			if(response){
				alert(`Registration Successful.`)

				//redirect
				navigate('/login')
			} else {
				alert('Something went wrong. Please try again')
			}
		})
			} else {
				alert(`User already exists`)
			}
		})
	}

  return (
    <Container>
      <Wrapper>
        <Title>CREATE AN ACCOUNT</Title>
        <Form onSubmit={(e) => registerUser(e) }>

        <Input placeholder="Username" 
          type="text"
          value={username}
          onChange={(e) => setUsername(e.target.value)}/>

          <Input placeholder="First Name" 
          type="text"
          value={fN}
          onChange={(e) => setFN(e.target.value)}/>
         
          <Input placeholder="Last Name"
          type="text"
          value={lN}
          onChange={(e) => setLN(e.target.value)} />
          
          <Input placeholder="Email"
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)} />
          
          <Input placeholder="Password"
          type="password"
          value={pw}
          onChange={(e) => setPW(e.target.value)} />
         
          <Input placeholder="Confirm Password"
          type="password"
          value={vpw}
          onChange={(e) => setVPW(e.target.value)} />

          <Linek><Link to="/login">Already have an Account? LOGIN</Link></Linek>

          <Button 
          type="submit"
          disabled={isDisabled}>CREATE</Button>
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Register;