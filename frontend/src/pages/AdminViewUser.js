import { useEffect, useState, Fragment, useContext } from 'react'
import { Container, Table, Button } from 'react-bootstrap'
import UserContext from './../UserContext'
import{useNavigate} from 'react-router-dom'
import Announcement from '../components/Announcement'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'




export default function AdminViewUser() {
	const navigate = useNavigate();

	const [allUser, setAllUser] = useState([])

	const { dispatch } = useContext(UserContext)

	const fetchData = () => {
		fetch(`http://localhost:4000/api/user/`, {
			method: "GET",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			dispatch({type: "USER", payload: true})

			setAllUser( response.map(user => {
				// console.log(user)

				return(
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.email}</td>
						<td>{user.firstName}</td>
                        <td>{user.lastName}</td>
						<td>{user.isAdmin ? "Admin" : "User"}</td>
						<td>
							{
								user.isAdmin ?
									<Button 
										className="btn btn-danger mx-2"
										onClick={ () => handleUser(user._id) }
									>
										USER
									</Button>
								:
									<Fragment>
										<Button 
											className="btn btn-success mx-2"
											onClick={ () => handleAdmin(user._id)}
										>
												ADMIN
										</Button>
										<Button 
											className="btn btn-secondary mx-2"
											onClick={ () => handleDelete(user._id) }
										>
											Delete
										</Button>
									</Fragment>
							}
						</td>
					</tr>
				)
			}))
		})
	}

	useEffect(() => {
		fetchData()

	}, [])

	const handleUser = (userId) =>{
		console.log(userId)
		fetch(`http://localhost:4000/api/user/${userId}/setAsUser`, {
			method: "PUT",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()

				alert('Admin converted to user!')
			}
		})
	}

	const handleAdmin = (userId) =>{
		console.log(userId)
		fetch(`http://localhost:4000/api/user/${userId}/setAsAdmin`, {
			method: "PUT",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('User converted to admin')
			}
		})
	}

	const handleDelete = (userId) =>{
		console.log(userId)
		fetch(`http://localhost:4000/api/user/${userId}/delete-user`, {
			method: "DELETE",
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(response => response.json())
		.then(response => {
			console.log(response)

			if(response){
				fetchData()
				
				alert('User successfully Deleted!')
			}
		})
	}
	const handleClickUserDashboard = () => {
        navigate('/products')
    };

	return(
       

		<Container className="container">
                 <Announcement />
                <Navbar />
			<h1 className="my-5 text-center">Admin User Dashboard</h1>
			<Button className="btn btn-success mx-2" onClick={handleClickUserDashboard}>Product Dashboard</Button>
			<Table>
				<thead>
					<tr>
						<th>ID</th>
						<th>Product Name</th>
						<th>Price</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ allUser }
				</tbody>
			</Table>
            <Footer />
		</Container>
	)
}