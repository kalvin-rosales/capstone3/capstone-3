import React, {useState} from 'react'
import styled from "styled-components"
import { ArrowBackIosRounded,ArrowForwardIosRounded } from '@material-ui/icons'
import shoe1 from './../images/shoe1.png'
import shoe2 from './../images/shoe2.png'
import shoe3 from './../images/shoe3.png'
import{useNavigate} from 'react-router-dom'



const Container = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    overflow: hidden;
    position: relative;

`
const Arrow = styled.div`
  width:50px;
  height: 50px;
  background-color: white;
  border-radius:50%;
  display: flex;
  align-items:center;
  justify-content: center;
  position: absolute;
  top: 0;
  bottom: 0;
  left: ${ props => props.direction === "left" && "10px"};
  right: ${ props => props.direction === "right" && "10px"};
  margin:auto;
  cursor: pointer;
  z-index:2 ;
`

const Wrapper = styled.div`
  height:100%;
  display: flex;
  transform: translateX(${props => props.slideIndex * -100}vw);
  transition: all 1s ease;
`
const Slide = styled.div`
  width:100vw;
  height: 100vh;
  display:flex;
  allign-items: center;
`
const ImgContainer = styled.div`
height: 100%
  flex: 1;
`

const Image = styled.img`
  height: 80%
`

const InfoContainer = styled.div`
  paddding: 50px;
  flex: 1;
`

const Title = styled.h1`
  font-size: 70px;
`
const Desc = styled.p`
  margin: 50px 0px;
  font-size: 20px;
  font-weight:500;
  letter-spacing: 10px;
`
const Button = styled.button`
  padding: 10px;
  font-size:20ox;
  background-color: transparent;
  cursor: pointer;
`


const Slider = () => {

  const navigate = useNavigate();

    const [slideIndex, setSlideIndex] = useState(0)



    const handleClick = (direction) => {
      if(direction ==="left"){
        setSlideIndex(slideIndex > 0 ? slideIndex-1 : 2)
      } else {
        setSlideIndex(slideIndex < 2 ? slideIndex +1 : 0)
      }
    };

    const handleClickShop = () => {
        navigate('/products')
    };

  return (
    <Container>
        <Arrow direction = "left" onClick={()=>handleClick("left")}>
          <ArrowBackIosRounded/>
            
        </Arrow>
          <Wrapper slideIndex = {slideIndex}>
            <Slide>
            <ImgContainer>
              <Image src={shoe1} className='img-fluid'/>
             </ImgContainer>
            <InfoContainer>
              <Title> New Arrivals</Title>
              <Desc>Fresh and new designs are out right now</Desc>
              <Button onClick={handleClickShop}>SHOP NOW</Button>

            </InfoContainer>
            </Slide>
            <Slide>
            <ImgContainer>
              <Image src={shoe2} className='img-fluid'/>
             </ImgContainer>
            <InfoContainer>
              <Title> It`s Hot</Title>
              <Desc>Fresh and new designs are out right now</Desc>
              <Button onClick={handleClickShop}>SHOP NOW</Button>

            </InfoContainer>
            </Slide>
            <Slide>
            <ImgContainer>
              <Image src={shoe3} className='img-fluid'/>
             </ImgContainer>
            <InfoContainer>
              <Title> Check It Out </Title>
              <Desc>Fresh and new designs are out right now</Desc>
              <Button onClick={handleClickShop}>SHOP NOW</Button>

            </InfoContainer>
            </Slide>
          </Wrapper>

        <Arrow direction = "right" onClick={()=>handleClick("right")}>
          <ArrowForwardIosRounded/>
            
        </Arrow>
    </Container>
  )
}

export default Slider
