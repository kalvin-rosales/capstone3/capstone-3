import { useEffect, useState, Fragment, useContext } from "react";
import styled from "styled-components";
import { popularProducts } from "../data";
import Product from "./Product";
import axios from "axios";
import AdminView from './../pages/AdminView'
import UserContext from './../UserContext'

const admin = localStorage.getItem('admin')
const token = localStorage.getItem('token')

const Container = styled.div`
  padding: 20px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const Wrapper = styled.div`
  padding: 20px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;`

const Products = ({ cat, filters, sort }) => {
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const { state, dispatch } = useContext(UserContext)

  useEffect(() => {
    const getProducts = async () => {
      if(admin ==="false"){
      try {
        const res = await axios.get(
          cat
            ? `http://localhost:4000/api/product?category=${cat}`
            : "http://localhost:4000/api/product"
        );
        setProducts(res.data);
        if(token !==null){
          dispatch({type:"USER", payload:true})
            }
      } catch (err) {}
    };
  }
    getProducts();
  }, [cat]);

  useEffect(() => {
    cat &&
      setFilteredProducts(
        products.filter((item) =>
          Object.entries(filters).every(([key, value]) =>
            item[key].includes(value)
          )
        )
      );
  }, [products, cat, filters]);

  useEffect(() => {
    if (sort === "newest") {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => a.createdAt - b.createdAt)
      );
    } else if (sort === "asc") {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => a.price - b.price)
      );
    } else {
      setFilteredProducts((prev) =>
        [...prev].sort((a, b) => b.price - a.price)
      );
    }
  }, [sort]);

  return (
  
      <Container>
    {
      admin === "false" ?
    
    <Fragment>

    <Wrapper>
      {cat
        ? filteredProducts.map((item) => <Product item={item} key={item.id} />)
        : products
            .slice(0, 8)
            .map((item) => <Product item={item} key={item.id} />)}
    </Wrapper>
  
    </Fragment>
      :
      <AdminView />
  }
  </Container>
  );
};

export default Products;