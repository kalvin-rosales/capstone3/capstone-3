import axios from "axios";

const BASE_URL = "http://localhost:4000/api/";
const token = localStorage.getItem("token")
//  const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyNDNiZmM3NmFlZjEyMjBlOTZiMTRhZiIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY0OTE0NzUxMiwiZXhwIjoxNjUxNzM5NTEyfQ.H6mTaYw_d7cyW7aYNc5IyXJZ4GBtGqgAD-ZvxhtOzSI"
//   JSON.parse(JSON.parse(localStorage.getItem("persist:root")).user).currentUser
//     .accessToken || "";

const user = JSON.parse(localStorage.getItem("persist:root"))?.user;
const currentUser = user && JSON.parse(user).currentUser;
// const TOKEN = currentUser?.accessToken;

export const publicRequest = axios.create({
  baseURL: BASE_URL,
});

// export const userRequest = axios.create({
//   baseURL: BASE_URL,
//   header: { token: `Bearer ${TOKEN}` },
// });